import { Modal } from "./Modal";
import { loginHtml } from "./modalsHtml";
import { addVisitHtml } from "./modalsHtml";
import * as axios from "axios";
import { Card } from "./Card";

export class Visit {
    constructor() {}

    authorization() {
        const newModalProps = {
            id: "modal",
            classList: ["modal", "big-modal"],
        };
        const newModal = new Modal(newModalProps);
        document.body.append(newModal.render(loginHtml));
        const signBtn = document.querySelector(".sign-btn");
        signBtn.addEventListener("click", newModal.openModal.bind(newModal));
        const login = document.getElementById("login");

        login.addEventListener("submit", () => {
            sessionStorage.setItem("userName", usrname.value);
            sessionStorage.setItem("password", psw.value);
        });
    }

    addVisit(response) {
        let cards = [];
        const header = document.querySelector(".header");
        let newBtn = document.createElement("button");
        newBtn.classList.add("create-btn");
        header.append(newBtn);
        let signBtn = document.querySelector(".sign-btn");
        signBtn.remove();
        let createBtn = document.querySelector(".create-btn");
        createBtn.textContent = "Создать визит";

        let { data: token } = response;
        sessionStorage.setItem("token", token);

        const newModalProps = {
            id: "modal",
            classList: ["modal", "big-modal"],
        };
        const newModal = new Modal(newModalProps);
        document.body.append(newModal.render(addVisitHtml));

        createBtn.addEventListener("click", newModal.openModal.bind(newModal));
        const visit = document.getElementById("visit");
        visit.addEventListener("submit", (e) => {
            e.preventDefault();
            // create new Card
            const testCard = new Card(
                document.getElementById("doctor").value,
                document.getElementById("urgency").value,
                document.getElementById("userName").value
            );
            console.log(testCard);
            axios
                .post("https://ajax.test-danit.com/api/cards", testCard)
                .then((response) => {
                    console.log(response.data.content);
                    return response;
                })
                .then((response) => {
                    cards.push({
                        id: response.data.id,
                        title: response.data.content._title,
                        urgency: response.data.content._urgency,
                    });
                    console.log("cards", cards);
                })
                .catch((error) => {
                    console.log(error);
                });
        });
    }
}