export const loginHtml = `<div class="login-content">
        <span class="close">&times;</span>
        <div class="container">
           <form id = "login" method="get" action="">
               <label for="usrname">Username</label>
               <input type="text" id="usrname" name="usrname" required>
               <label for="psw">Password</label>
               <input type="password" id="psw" name="psw" required>
               <input type="submit" value="Submit" id="submit">
            </form>
        </div>
    </div>`;

export const addVisitHtml = `<div class="add-content">
        <span class="close">&times;</span>
        <div class="container">
           <form id = "visit" method="get" action="">
               <label for="doctor">Специалист</label>
               <select name="doctor" id="doctor">
               <option value="Therap">Терапевт</option>
               <option value="Cardio">Кардиолог</option>
               <option value="Dentist">Стоматолог</option>
               </select>
               <label for="urgency">Срочность</label>
               <select name="urgency" id="urgency">
               <option value="normal">Обычная</option>
               <option value="priority">Приоритетная</option>
               <option value="urgent">Неотложная</option>
               </select>
               <label for="purpose"></label>
               <textarea id="purpose" name="purpose" required>цель визита</textarea>
               <label for="userName"></label>
               <input type="text" id="userName" name="userName" value="Ф.И.О." required>
               <input type="submit" value="Создать" id="submit">
            </form>
        </div>
    </div>`;