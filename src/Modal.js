export class Modal {
    constructor({ id, classList }) {
        this.id = id;
        this.classList = [...classList];
        this.elem = null;
    }

    render(html) {
        this.elem = document.createElement("div");
        const { elem } = this;
        elem.id = this.id;
        elem.classList = this.classList.join(" ");
        elem.innerHTML = html;
        elem.addEventListener("click", this.closeModal.bind(this));
        return elem;
    }

    openModal() {
        this.elem.classList.add("active");
    }

    closeModal({ target }) {
        if (target.classList.contains("close")) {
            this.elem.classList.remove("active");
        }
    }
}