export class Dom {
    constructor(idRoot) {
        this._idRoot = idRoot;
        this._id = 0;
    }

    /**
     *
     * @param stroka
     * @param id
     */
    putString(stroka, id) {
        const el = document.getElementById(id);
        let div = document.createElement("div");
        if (id === `${this._idRoot}`) {
            div.setAttribute("id", `${this._id}`);
        } else {
            div.setAttribute("id", `${id}.1`);
        }
        div.innerHTML = stroka;
        el.append(div);
    }

    /**
     * Вставляет строку после элемента с idRoot
     * и присваивает новому элементу  id++
     * @param stroka - строковое выражение
     */
    set string(stroka) {
        this.putString(stroka, `${this._idRoot}`);
        this._id++;
    }

    /**
     * Вставляет строку после элемента с id и
     * добавляет к id нового элемента префикс вложенности ".1"
     * @param stroka - строковое выражение
     * @param id - number
     */
    stringSetAfterId(stroka, id) {
        this.putString(stroka, `${id}`);
    }

    cardToHtml(cardsY) {
        for (let {id, content} of cardsY) {
            const root = document.getElementById('root');
            const div = document.createElement('div')
            div.setAttribute("id", id);
            let {_title, _urgency, _userName} = content;
            div.innerHTML = `<div>Врач: ${_title}</div>
                             <div>Срочность: ${_urgency}</div>;
                             <div>Ф.И.О: ${_userName}</div>`;
            root.append(div);
            console.log(id, content);
        }
    }

}

