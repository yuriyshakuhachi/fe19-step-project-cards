import {Dom} from "./Dom";
import {Visit} from "./Visit";
import * as axios from "axios";
import {Card} from "./Card";

let cardsY = [];

const cardsDom = new Dom("root");
const newVisit = new Visit();
newVisit.authorization();
/**
 *Авторизация
 * получаем токен
 */
axios
    .post("https://ajax.test-danit.com/api/cards/login", {
        email: sessionStorage.getItem("userName"),
        password: sessionStorage.getItem("password"),
    })
    .then((response) => {

        newVisit.addVisit(response);
        /**
         *получаем список добавленных карточек
         * и пишем его в масссив
         */
        const token = sessionStorage.getItem(("token"));
        axios.defaults.headers.common.Authorization = `Bearer ${token}`
        axios("https://ajax.test-danit.com/api/cards")
            .then((response) => {
                let {data} = response;
                cardsY = [...data];
                cardsDom.cardToHtml(cardsY)
            })
            .catch((error) => {
                console.log(error);
            });
    })
    .catch((error) => {
        cardsDom.string = "No items have been added";
        console.log("error", error);
    });

/**
 *
 * добавляем токен
 */

let token = sessionStorage.getItem("token");
axios.defaults.headers.common.Authorization = `Bearer ${token}`;


// axios
//     .post("https://ajax.test-danit.com/api/cards/login", {
//         email: localStorage.getItem("userName"),
//         password: localStorage.getItem("password"),
//     })
//     .then((response) => {
//         const signBtn = document.querySelector(".sign-btn");
//         signBtn.textContent = "Создать визит";
//         let { data: token } = response;
//     })
//     .catch((error) => {
//         cardsDom.string = "No items have been added";
//         console.log(error);
//     });

// const users = [
//     {name: 'Andrew', email: 'madpilot911@gmail.com', password: '13413'},
//     {name: 'Yurii', email: 'yuriyshakuhachi@gmail.com', password: 'edginoreh'}
// ];

// axios.post('https://ajax.test-danit.com/api/cards/login', {
//     "email": "yuriyshakuhachi@gmail.com",
//     "password": "edginoreh"
// })
//     .then(response => {
//         console.log(response);
//         let {data: token} = response
//         console.log(token);
//     })
//     .catch(error => {
//         console.log(error);
//     });


/**
 * добавляем карточку
 */
//

// axios
//     .post("https://ajax.test-danit.com/api/cards", {
//         title: "Визит к зубойнику",
//         description: "Плановый визит",
//         doctor: "Белобров",
//         bp: "24",
//         age: 49,
//         weight: 104,
//     })
//     .then((response) => {
//         console.log(response.data);
//     })
//     .catch((error) => {
//         console.log(error);
//     });

//Пушит айди и тайтл в переменную

// axios
//     .post("https://ajax.test-danit.com/api/cards", {
//         title: "Визит к...",
//         description: "Плановый визит",
//         doctor: "Белобров",
//         bp: "24",
//         age: 49,
//         weight: 104,
//     })
//     .then((response) => {
//         console.log(response.data.content);
//         return response;
//     })
//     .then((response) =>
//         cards.push({ id: response.data.id, title: response.data.content.title })
//     )
//     .catch((error) => {
//         console.log(error);
//     });

// console.log(cards);

/**
 *удаляем карточку
 */

// axios
//     .delete(`https://ajax.test-danit.com/api/cards/${3549}`)
//     .then((response) => {
//         console.log("deleted", response.data);
//     })
//     .catch((error) => {
//         console.log(error);
//     });

// axios("https://ajax.test-danit.com/api/cards/3549")
//     .then((response) => {
//         console.log(response);
//     })
//     .catch((error) => {
//         console.log(error);
//     });
